package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Arrays;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    
    public String readInput(String prompt) {
        System.out.println(prompt);
        String input = sc.next().toLowerCase();
        return input;
    }   

    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {

        sc = new Scanner(System.in);

        int gameRound = 1;
        int playerScore = 0;
        int computerScore = 0;

        while (true) {

            System.out.println("Let's play round " + gameRound);

            String playerChoice = readInput("Your choice (Rock/Paper/Scissors)?");

            while (true) {
                if (rpsChoices.contains(playerChoice)) {
                    break;
                } else {
                    System.out.println("I do not understand " + playerChoice + ". Could you try again?");
                    playerChoice = readInput("Your choice (Rock/Paper/Scissors)?");
                }
            }

            String aiChoice = rpsChoices.get((int) (Math.random() * rpsChoices.size()));

            String choices = ("Human chose " + playerChoice + ", computer chose " + aiChoice + ".");

            if (playerChoice.equals(aiChoice)) {
                System.out.println(choices + " It's a tie");
            } else if (playerChoice.equals("rock")) {
                if (aiChoice.equals("paper")) {
                    System.out.println(choices + " Computer wins.");
                    computerScore += 1;
                } else {
                    System.out.println(choices + " Human wins.");
                    playerScore += 1;
                }
            } else if (playerChoice.equals("paper")) {
                if (aiChoice.equals("scissors")) {
                    System.out.println(choices + " Computer wins.");                    
                    computerScore += 1;
                } else {
                    System.out.println(choices + " Human wins.");
                    playerScore += 1;
                }
            } else if (playerChoice.equals("scissors")) {
                if (aiChoice.equals("rock")) {
                    System.out.println(choices + " Computer wins.");
                    computerScore += 1;
                } else {
                    System.out.println(choices + " Human wins.");
                    playerScore += 1;
                }
            }
            System.out.println("Score: human " + playerScore + ", computer " + computerScore);   
        
            gameRound += 1;  

            String cont = readInput("Do you wish to continue playing? (y/n)? ");

            if (cont.equals("n")) {
                System.out.println("Bye bye :)"); 
                break;
            } else {
                if (!cont.equals("y")) {
                    System.out.println("I do not understand " + cont + ". Could you try again?");
                    cont = readInput("Do you wish to continue playing? (y/n)? ");
                } 
            }
        } 
    } 
}
