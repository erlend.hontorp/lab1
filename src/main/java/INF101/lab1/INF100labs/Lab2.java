package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.List;

public class Lab2 {
    
    public static void main(String[] args) {

        findLongestWords("Game", "Action", "Champion");

        isLeapYear(0);

        isEvenPositiveInt(0);
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {

        int max = Math.max(Math.max(word1.length(), word2.length()), word3.length());

        List<String> ord = new ArrayList<>();

        if (word1.length() == max) {
            ord.add(word1);
        }
        if (word2.length() == max) {
            ord.add(word2);
        }
        if (word3.length() == max) {
            ord.add(word3);
        }
 
        for (int i = 0; i < ord.size(); i++) {
 
            System.out.println(ord.get(i));
        
        }
       
    }

    public static boolean isLeapYear(int year) {
        
        if (year % 400 == 0 && year % 100 == 0) {
            return true;
        } else if (year % 4 ==0 && year % 100 != 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isEvenPositiveInt(int num) {

        if (num % 2 == 0 && num > -1) {
            return true;
        } else {
            return false;
        }
    }
}
