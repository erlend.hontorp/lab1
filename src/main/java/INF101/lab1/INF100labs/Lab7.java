package INF101.lab1.INF100labs;

import java.util.ArrayList;
// import java.util.stream.IntStream;

public class Lab7 {

    public static void main(String[] args) {
        
        removeRow(null, 0);

        allRowsAndColsAreEqualSum(null);

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
        System.out.println(grid);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {

        int rowCount = grid.size();
        int colCount = grid.get(0).size();

        // Sjekker sum for hver rad
        for (int i = 1; i < rowCount; i++) {
            if (rowSum(grid.get(i)) != rowSum(grid.get(0))) {
                return false;
            }
        }

        // Sjekker sum for hver kolonne
        for (int j = 0; j < colCount; j++) {
            if (colSum(grid, j) != colSum(grid, 0)) {
                return false;
            }
        }

        // Hvis alle summer er like
        return true;
    }

    // Hjelpefunksjon for å beregne summen av en rad
    private static int rowSum(ArrayList<Integer> row) {
        return row.stream().mapToInt(Integer::intValue).sum();
    }

    // Hjelpefunksjon for å beregne summen av en kolonne
    private static int colSum(ArrayList<ArrayList<Integer>> grid, int colIndex) {
        return grid.stream().mapToInt(row -> row.get(colIndex)).sum();
    }
}
