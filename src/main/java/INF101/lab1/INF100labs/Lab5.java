package INF101.lab1.INF100labs;

import java.util.ArrayList;
// import java.util.List;
//import java.util.List;

public class Lab5 {
    
    public static void main(String[] args) {
       
        multipliedWithTwo(null);

        removeThrees(null);

        uniqueValues(null);

        addList(null, null);

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        
        ArrayList<Integer> nyListe = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {

            nyListe.add(list.get(i)*2);
    
        }
        return nyListe;    
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {

        ArrayList<Integer> nyListe = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {

            if (list.get(i) != 3) {
                nyListe.add(list.get(i));
            }
    
        }
        return nyListe;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        
        ArrayList<Integer> nyListe = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {

            
            if (nyListe.contains(list.get(i)) == false) {
                nyListe.add(list.get(i));
            }
    
        }
        return nyListe;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
                
        for (int i = 0; i < a.size(); i++) {

            int nyttTall = (a.get(i) + b.get(i));
            a.set(i, nyttTall);
            
        }
        System.out.println(a);
    }

}