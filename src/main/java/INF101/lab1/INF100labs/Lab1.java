package INF101.lab1.INF100labs;

import java.util.Scanner;

public class Lab1 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        
        task1();

        task2();
    }

    public static void task1() {
        System.out.println("Hei, det er meg, datamaskinen.");
        System.out.println("Hyggelig å se deg her.");
        System.out.println("Lykke til med INF101!");
    }

    public static void task2() {
        sc = new Scanner(System.in); // Do not remove this line

        System.out.println("Hva er ditt navn?");
        String navn = sc.nextLine();
        System.out.println("Hva er din adresse?");
        String gate = sc.nextLine();
        System.out.println("Hva er ditt postnummer og poststed?");
        String poststed = sc.nextLine();

        System.out.println(navn + "s adresse er:\n\n" + navn + "\n" + gate + "\n" + poststed);    
            
    }

    public static String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }


}
