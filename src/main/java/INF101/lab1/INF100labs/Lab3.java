package INF101.lab1.INF100labs;

public class Lab3 {
    
    public static void main(String[] args) {

        multiplesOfSevenUpTo(0);

        multiplicationTable(0);

        crossSum(35);

    }

    public static void multiplesOfSevenUpTo(int n) {
        
        for (int i = 1; i < n; i++) {

            int x = (i*7);

            if (x <= n) {

                System.out.println(x);    
            }
        }
    }

    public static void multiplicationTable(int n) {
        
        for (int i = 1; i <= n; i++) {

            System.out.print(i + ": ");
            
            for (int j = 1; j <= n; j++) {
                
                int x = i * j;
                
                System.out.print(x + " ");

            }

            System.out.println();
        }

    }

    public static int crossSum(int num) {

        int sum = 0;
        while (num > 0) {
            sum = sum + num % 10;
            num = num / 10;
        }
        return sum;

    }

}